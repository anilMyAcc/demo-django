import pymongo

def connection(**creds):
    host: str = creds["host"]
    port: int = creds.get("port", 27017)
    user: str = creds["user"]
    password: str = creds["password"].replace("@", "%40")
    database: str = creds["database"]
    authsource: str = creds.get("authsource")

    if ((not host) or (not port) or
        (not user) or (not password) or (not database)):
            raise Exception
    
    mongo_url = f"mongodb://{user}:{password}@{host}:{port}/{database}"
    if authsource:
        mongo_url = f"{mongo_url}?authSource={authsource}"
    try:
        client = pymongo.MongoClient(mongo_url, serverSelectionTimeoutMS=10*1000)
        db = client[database]
        return (client, db)
    except Exception as e:
        print(e)
        raise Exception

