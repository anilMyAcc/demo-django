# demo-django

Its a sample django project to demonstrate rest apis.

### Installation

This application requires git, Docker and docker-compose to run.

```
git clone https://gitlab.com/anilMyAcc/demo-django.git

cd demo-django

./deploy.sh
    - This script stops already running containers and builds and starts new containers.
    - Their are two containers.
        - One to for django application [port - 8000].
        - Another for database i.e., mongodb [port - 27017].
```


### Usage

- API end points avaliable
```
    - <host>:<port>/apidocs/
        - Displays swagger UI to view and test REST API's.
    - <host>:<port>/swagger.json
        - To view swagger data in json format.
    - <host>:<port>/redoc/
        - To view API documentation.
    - <host>:<port>/admin/
        - Displays django admin dashboard.
    - <host>:<port>/members/
        - Demo API to display members activity log.
```

NOTE: To test API's please use apidocs/ endpoint and try it out.