#!/usr/bin/env bash

# Stopping containers
docker-compose -f deployment/docker-compose.yml down
# Removing unwanted 
docker volume prune -f
# Staring containers
docker-compose -f deployment/docker-compose.yml up --build -d