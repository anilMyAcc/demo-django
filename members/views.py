from django.shortcuts import render
from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import MembersModel
from .serializers import MemberSerializers
from .services import get_members_activity


class MembersView(APIView):
    """
        Members API's
    """

    @swagger_auto_schema(
        responses={200: MemberSerializers},
        tags=['Members'],
        operation_summary="""
            Description: All members activity period.
        """,
        operation_description="""
            Returns lists of members activity period.
        """
    )
    def get(self, request):
        """
            Returns lists of members activity period.
        """
        avt_list = []
        try:
            avt_list = get_members_activity()
        except Exception as e:
            print(e)

        serializer = MemberSerializers(data=avt_list, many=True)
        serializer.is_valid()
    
        return Response({
            "status": 200 if avt_list else 204,
            "message": "Data retrived successfully" if avt_list else "No Data found!",
            "data": serializer.data
        })