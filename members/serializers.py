from rest_framework import serializers
from .models import MembersModel, ActivityPeriodModel

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityPeriodModel
        fields = '__all__'

class MemberSerializers(serializers.ModelSerializer):

    activity_periods = ActivitySerializer(many=True)

    class Meta:
        model = MembersModel
        fields = ('id', 'real_name', 'tz', 'activity_periods')