from django.db import models

class MembersModel(models.Model):
    id = models.TextField(primary_key=True)
    real_name = models.TextField()
    tz = models.TextField()

class ActivityPeriodModel(models.Model):
    start_time = models.TextField()
    end_time = models.TextField()