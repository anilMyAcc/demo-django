from django.contrib import admin

# Register your models here.
from .models import MembersModel, ActivityPeriodModel

admin.site.register(MembersModel)
admin.site.register(ActivityPeriodModel)