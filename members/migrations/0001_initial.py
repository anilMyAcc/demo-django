# Generated by Django 3.0.7 on 2020-06-03 18:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ActivityPeriodModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.TextField()),
                ('end_time', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='MembersModel',
            fields=[
                ('id', models.TextField(primary_key=True, serialize=False)),
                ('real_name', models.TextField()),
                ('tz', models.TextField()),
            ],
        ),
    ]
