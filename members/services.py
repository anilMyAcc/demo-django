from utils import db

def get_members_activity():
    """
    Return data retriverd from collection
    [activty log] in database [members]
    """
    client, db_cur = db.mongoConn(
        host='localhost',
        port=27017,
        user='mongoadmin',
        password='admin@27017',
        database='members',
        authsource='admin'
    )
    doc = db_cur["activity_log"].find()
    client.close()
    return list(doc)